# vue-project

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur).

## Type Support for `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates.

However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can run `Volar: Switch TS Plugin on/off` from VSCode command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```
2019年2月入职纬创，即将满三年了，三年中参与了微服务养老核心项目作战地图，智能报价，理赔查询，理赔申请，理赔补传 等项目 ，在项目中我利用自己的技术优势，不断优化项目的目录和组织结构，寻找合适解决方案，力求最好的完成项目中的难点，使项目正常上线，在今年还从零到一搭建了企业专区的前端框架，带领前端小伙伴一起工作，和提升技术能力；在这套前端框架中包括了pc端和移动端展示平台，和组件平台，使组件库可以在pc和移动端同时使用，还用业余时间优化代码，提高代码性能，参与前端团队招聘面试等工作；
在工作中，及时与甲方沟通工作细节，避免工作中因为沟通不畅带来的问题，工作认真负责，不逃避责任；遵守公司的各项规章制度，没有迟到早退，及时解决问题，利用技术影响力，影响团队积极解决问题，创造积极价值，给甲方公司留下好印象