import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import '@/style/index.scss'
import installIcons from '@/components/icons'
import 'virtual:svg-icons-register'
import App from './App.vue'
import router from './router'

const app = createApp(App)
installIcons(app)
app.use(ElementPlus)
app.use(createPinia())
app.use(router)


app.mount('#app')
